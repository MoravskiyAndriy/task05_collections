package com.moravskiyandriy;

import com.moravskiyandriy.commands.Menu;
import com.moravskiyandriy.binarytree.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.put(8, "Kabul");
        tree.put(6, "Tirana");
        tree.put(5, "Canberra");
        tree.put(12, "Vienna");
        tree.put(10, "Baku");
        tree.put(3, "Nassau");
        tree.put(2, "Manama");
        tree.put(11, "Algiers");
        tree.put(7, "Luanda");
        tree.put(4, "Yerevan");
        tree.print();
        logger.info("\nWay to object for deleting:\n");
        tree.remove(2);
        tree.print();
        Menu menu = new Menu(tree);
        Scanner scanner = new Scanner(System.in);
        while (true) {
            logger.info("\nMenu: ");
            menu.printMenu();
            logger.info("Your choise:");
            String choise = scanner.nextLine();
            if (choise.equals("quit")) {
                break;
            }
            menu.chose(Integer.valueOf(choise));
        }
    }
}
