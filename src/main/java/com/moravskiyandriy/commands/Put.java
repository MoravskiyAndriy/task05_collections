package com.moravskiyandriy.commands;

import com.moravskiyandriy.binarytree.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Put implements Command {
    private static final Logger logger = LogManager.getLogger(Put.class);
    private BinaryTree binaryTree;

    Put(final BinaryTree binaryTree) {
        this.binaryTree = binaryTree;
    }

    public void execute() {
        Scanner input = new Scanner(System.in);
        logger.info("Input key (Integer): ");
        String key = input.nextLine();
        logger.info("Input value (String): ");
        String value = input.nextLine();
        binaryTree.put(Integer.valueOf(key), value);
    }
}
