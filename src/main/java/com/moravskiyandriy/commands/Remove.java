package com.moravskiyandriy.commands;

import com.moravskiyandriy.binarytree.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Remove implements Command {
    private static final Logger logger = LogManager.getLogger(Remove.class);
    private BinaryTree binaryTree;

    Remove(final BinaryTree binaryTree) {
        this.binaryTree = binaryTree;
    }

    public void execute() {
        Scanner input = new Scanner(System.in);
        logger.info("Input key (Integer): ");
        int key = input.nextInt();
        binaryTree.remove(key);
    }
}
