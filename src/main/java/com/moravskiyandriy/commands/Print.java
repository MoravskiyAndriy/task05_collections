package com.moravskiyandriy.commands;

import com.moravskiyandriy.binarytree.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Print implements Command {
    private static final Logger logger = LogManager.getLogger(Print.class);
    private BinaryTree binaryTree;

    Print(final BinaryTree binaryTree) {
        this.binaryTree = binaryTree;
    }

    public void execute() {
        logger.info("Binary tree: ");
        binaryTree.print();
        logger.info("size: " + binaryTree.size());
    }
}
