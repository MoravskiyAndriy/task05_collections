package com.moravskiyandriy.commands;

import com.moravskiyandriy.binarytree.BinaryTree;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private CommandSwitcher commandSwitcher;
    private List<String> menu = new ArrayList<>();

    public Menu(BinaryTree tree) {
        commandSwitcher = new CommandSwitcher();
        commandSwitcher.addCommand(new Put(tree));
        menu.add("0. Put");
        commandSwitcher.addCommand(new GetByKey(tree));
        menu.add("1. GetByKey");
        commandSwitcher.addCommand(new Print(tree));
        menu.add("2. Print tree");
        commandSwitcher.addCommand(new Remove(tree));
        menu.add("3. Remove");
        commandSwitcher.addCommand(new GetAllKeysAndValues(tree));
        menu.add("4. Get all keys and values.");
    }

    public void chose(final Integer number) {
        commandSwitcher.execute(number);
    }

    public void printMenu() {
        for (String s : menu) {
            System.out.println(s);
        }
    }
}
