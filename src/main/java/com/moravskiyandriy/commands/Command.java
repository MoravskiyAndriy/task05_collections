package com.moravskiyandriy.commands;

public interface Command {
    void execute();
}
