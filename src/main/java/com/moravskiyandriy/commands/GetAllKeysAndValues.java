package com.moravskiyandriy.commands;

import com.moravskiyandriy.binarytree.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetAllKeysAndValues implements Command {
    private static final Logger logger = LogManager.getLogger(GetAllKeysAndValues.class);
    private BinaryTree binaryTree;

    GetAllKeysAndValues(final BinaryTree binaryTree) {
        this.binaryTree = binaryTree;
    }

    public void execute() {
        StringBuilder msg = new StringBuilder();
        logger.info("Keys: ");
        for (Integer key : binaryTree.keySet()) {
            msg.append(key).append(" ");
        }
        logger.info(msg + ".");
        msg = new StringBuilder();
        logger.info("Values: ");
        for (String value : binaryTree.values()) {
            msg.append(value).append(" ");
        }
        logger.info(msg + ".");
    }
}
