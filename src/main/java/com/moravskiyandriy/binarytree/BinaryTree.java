package com.moravskiyandriy.binarytree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class BinaryTree implements Map<Integer, String> {
    private Node root;
    private static final Logger logger = LogManager.getLogger(BinaryTree.class);
    private static final String CONST_ONE_SPACE = " ";
    private static Set<String> valuesSet;
    private static Set<Integer> keysSet;
    private static int size = 0;

    public BinaryTree() {
        setRoot(null);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean containsKey(Object o) {
        return get(o) != null;
    }

    public boolean containsValue(Object o) {
        return !Objects.isNull(getRoot());
    }

    public void putAll(Map<? extends Integer, ? extends String> map) {
        for (Map.Entry<? extends Integer, ? extends String> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public void clear() {
        setRoot(null);
    }

    private void checkAllForValues(Node node) {
        if (!Objects.isNull(node)) {
            valuesSet.add(node.getValue());
        }
        if (!Objects.isNull(node.getLeftNode())) {
            checkAllForValues(node.getLeftNode());
        }
        if (!Objects.isNull(node.getRightNode())) {
            checkAllForValues(node.getRightNode());
        }
    }

    private void checkAllForKeys(Node node) {
        if (!Objects.isNull(node)) {
            keysSet.add(node.getKey());
        }
        if (!Objects.isNull(node.getLeftNode())) {
            checkAllForKeys(node.getLeftNode());
        }
        if (!Objects.isNull(node.getRightNode())) {
            checkAllForKeys(node.getRightNode());
        }
    }

    public Set<Integer> keySet() {
        keysSet = new HashSet<>();
        checkAllForKeys(getRoot());
        return keysSet;
    }

    public Collection<String> values() {
        valuesSet = new HashSet<>();
        checkAllForValues(getRoot());
        return valuesSet;
    }

    public Set<Map.Entry<Integer, String>> entrySet() {
        return new HashSet<>();
    }

    private Node getRoot() {
        return root;
    }

    private void setRoot(Node root) {
        this.root = root;
    }

    public String put(Integer key, String value) {
        put(key, value, getRoot());
        return get(key);
    }

    private void put(Integer key, String value, Node node) {
        if (Objects.isNull(key) || Objects.isNull(value)) {
            logger.info("There are nulls");
            return;
        }
        if (Objects.isNull(node)) {
            setRoot(new Node(key, value));
            size++;
            return;
        }
        if (node.getKey().equals(key)) {
            node.setValue(value);
        } else if (node.getKey() > key) {
            if (node.getLeftNode() != null) {
                put(key, value, node.getLeftNode());
            } else {
                node.setLeftNode(new Node(key, value));
                size++;
            }
        } else if (node.getKey() < key) {
            if (node.getRightNode() != null) {
                put(key, value, node.getRightNode());
            } else {
                node.setRightNode(new Node(key, value));
                size++;
            }
        }
    }

    public String get(Object o) {
        Integer key = (Integer) o;
        Node temp = root;
        while (!temp.getKey().equals(key)) {
            if (temp.getKey() < key) {
                temp = temp.getLeftNode();
            } else if (temp.getKey() > key) {
                temp = temp.getRightNode();
            }
            if (temp == null) {
                return null;
            }
        }
        return temp.toString();
    }

    private String getNodes(Integer key) {
        Node temp = root;
        while (!temp.getKey().equals(key)) {
            if (temp.getKey() < key) {
                temp = temp.getRightNode();
            } else if (temp.getKey() > key) {
                temp = temp.getLeftNode();
            }
            if (Objects.isNull(temp)) {
                return "No object with such key found.";
            }
        }
        return "Nodes: " + temp.getLeftNode() + "  " + temp.getRightNode();
    }

    private Node getNodeInChain(Node node) {
        Node upperNode = node;
        Node lowerNode = node;
        Node current = node.getRightNode();
        while (current != null) {
            upperNode = lowerNode;
            lowerNode = current;
            current = current.getLeftNode();
        }
        if (lowerNode != node.getRightNode()) {
            upperNode.setLeftNode(lowerNode.getRightNode());
            lowerNode.setRightNode(node.getRightNode());
        }
        return lowerNode;
    }

    private Node[] getNodeToDelete(Object o) {
        Integer key = (Integer) o;
        if (Objects.isNull(key)) {
            logger.info("KeyIsNull");
            return null;
        }
        if (Objects.isNull(getRoot())) {
            logger.info("RootIsNull");
            return null;
        }
        Node toBeDeleted = getRoot();
        Node upperNode = getRoot();

        int iterator = 0;
        while (!toBeDeleted.getKey().equals(key)) {
            logger.info("STEP " + ++iterator + ":" + CONST_ONE_SPACE + toBeDeleted.getKey());
            upperNode = toBeDeleted;
            if (toBeDeleted.getKey() < key) {
                toBeDeleted = toBeDeleted.getRightNode();
                if (Objects.isNull(toBeDeleted)) {
                    logger.warn("No object with such key found.");
                    return null;
                }
                continue;
            }
            if (toBeDeleted.getKey() > key) {
                toBeDeleted = toBeDeleted.getLeftNode();
                if (Objects.isNull(toBeDeleted)) {
                    logger.warn("No object with such key found.");
                    return null;
                }
            }
        }
        logger.info("Object founded and deleted: " + toBeDeleted.getKey());
        return new Node[]{toBeDeleted, upperNode};
    }

    private void setNodeOnCondition(Node toBeDeleted, Node upperNode, Node node) {
        if (toBeDeleted.equals(getRoot())) {
            setRoot(node);
            size--;
        } else if (!Objects.isNull(upperNode.getLeftNode()) && upperNode.getLeftNode().equals(toBeDeleted)) {
            upperNode.setLeftNode(node);
            size--;
        } else if (!Objects.isNull(upperNode.getRightNode()) && upperNode.getRightNode().equals(toBeDeleted)) {
            upperNode.setRightNode(node);
            size--;
        }
    }

    public String remove(Object o) {
        Node[] nodesForDelete = getNodeToDelete(o);
        Node toBeDeleted = null;
        Node upperNode = null;
        try {
            toBeDeleted = nodesForDelete[0];
            upperNode = nodesForDelete[1];
        } catch (NullPointerException e) {
            logger.info("No object with such key found.");
            return "No object with such key found.";
        }

        if (Objects.isNull(toBeDeleted.getLeftNode()) && Objects.isNull(toBeDeleted.getRightNode())) {
            setNodeOnCondition(toBeDeleted, upperNode, null);
        } else if (Objects.isNull(toBeDeleted.getRightNode())) {
            setNodeOnCondition(toBeDeleted, upperNode, toBeDeleted.getLeftNode());
        } else if (Objects.isNull(toBeDeleted.getLeftNode())) {
            setNodeOnCondition(toBeDeleted, upperNode, toBeDeleted.getRightNode());
        } else {
            Node nodeInChain = getNodeInChain(toBeDeleted);
            setNodeOnCondition(toBeDeleted, upperNode, nodeInChain);
            nodeInChain.setLeftNode(toBeDeleted.getLeftNode());
        }
        return "OK";
    }

    public void print() {
        print(root);
    }

    private void print(final Node node) {
        if (node != null) {
            logger.info(node.getKey() + CONST_ONE_SPACE + node.getValue() + ": " + getNodes(node.getKey()));
            print(node.getLeftNode());
            print(node.getRightNode());
        }
    }

    private class Node {
        private Integer key;
        private String value;
        private Node leftNode;
        private Node rightNode;

        Node(final Integer key, final String value) {
            setKey(key);
            setValue(value);
            setLeftNode(null);
            setRightNode(null);
        }

        @Override
        public String toString() {
            return "{" +
                    "key=" + key +
                    ", value='" + value + "'} ";
        }

        Integer getKey() {
            return key;
        }

        void setKey(final Integer key) {
            this.key = key;
        }

        String getValue() {
            return value;
        }

        void setValue(final String value) {
            this.value = value;
        }

        Node getLeftNode() {
            return leftNode;
        }

        void setLeftNode(final Node leftNode) {
            this.leftNode = leftNode;
        }

        Node getRightNode() {
            return rightNode;
        }

        void setRightNode(final Node rightNode) {
            this.rightNode = rightNode;
        }
    }
}
