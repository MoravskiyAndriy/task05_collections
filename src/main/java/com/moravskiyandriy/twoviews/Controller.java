package com.moravskiyandriy.twoviews;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Controller {
    private static final Logger logger = LogManager.getLogger(Controller.class);

    void action(Integer number) {
        logger.info("action number= " + number);
    }
}
