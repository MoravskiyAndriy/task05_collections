package com.moravskiyandriy.twoviews;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View2MenuWithHashTable {
    private static final Logger logger = LogManager.getLogger(View2MenuWithHashTable.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, ForExecution> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private View2MenuWithHashTable() {
        controller = new Controller();
        menu = new LinkedHashMap<>();

        menu.put("1", "  1 - perform action1");
        menu.put("2", "  2 - perform action2");
        menu.put("3", "  3 - perform action3");
        menu.put("4", "  4 - perform action4");
        menu.put("5", "  5 - perform action5");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::action1);
        methodsMenu.put("2", this::action2);
        methodsMenu.put("3", this::action3);
        methodsMenu.put("4", this::action4);
        methodsMenu.put("5", this::action5);

    }

    private void action1() {
        controller.action(1);
    }

    private void action2() {
        controller.action(2);
    }

    private void action3() {
        controller.action(3);
    }

    private void action4() {
        controller.action(4);
    }

    private void action5() {
        controller.action(5);
    }

    private void showMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void show() {
        String keyMenu;
        do {
            showMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                continue;
            }
        } while (!keyMenu.equals("Q"));
    }

    public static void main(String[] args) {
        new View2MenuWithHashTable().show();
    }
}
