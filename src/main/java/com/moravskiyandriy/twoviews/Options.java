package com.moravskiyandriy.twoviews;

enum Options {
    OPTION1(1), OPTION2(2), OPTION3(3), OPTION4(4), OPTION5(5), OPTION6(6);
    public int number;

    Options(Integer number) {
        this.number = number;
    }
}
