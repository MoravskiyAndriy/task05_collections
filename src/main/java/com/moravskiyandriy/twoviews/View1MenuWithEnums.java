package com.moravskiyandriy.twoviews;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View1MenuWithEnums {
    private static final Logger logger = LogManager.getLogger(View1MenuWithEnums.class);
    private static final int NUMBER = 6;
    private Controller controller = new Controller();

    private void choose(Options o) {
        switch (o) {
            case OPTION1:
                controller.action(Options.OPTION1.number);
                break;
            case OPTION2:
                controller.action(Options.OPTION2.number);
                break;
            case OPTION3:
                controller.action(Options.OPTION3.number);
            case OPTION4:
                controller.action(Options.OPTION4.number);
            case OPTION5:
                controller.action(Options.OPTION5.number);
                break;
            case OPTION6:
                controller.action(Options.OPTION6.number);
                break;
        }
    }

    public static void main(String[] args) {
        logger.info("Menu");
        while (true) {
            for (int i = 1; i < NUMBER; i++) {
                logger.info(i + ".Option " + i);
            }
            logger.info(NUMBER + ".Quit ");
            logger.info("Chose option: ");
            Scanner scanner = new Scanner(System.in);
            String choise = scanner.nextLine();
            if (Integer.valueOf(choise).equals(NUMBER)) {
                break;
            }
            for (Options option : Options.values()) {
                if (option.number == Integer.valueOf(choise)) {
                    new View1MenuWithEnums().choose(option);
                }
            }
        }
    }
}
